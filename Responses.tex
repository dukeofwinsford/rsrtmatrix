\documentclass[sort&compress]{elsarticle}

\usepackage[dvipsnames]{xcolor}
\usepackage[a4paper]{geometry}
\usepackage{enumitem}
\usepackage{pifont} % Checkmark, X
\def\OK{{\large\color{ForestGreen}\ding{52}}}
\def\NOTOK{{\large\color{Red}\ding{55}}}
\newcommand{\RESPONSE}[1]{{\color{blue}{#1}}}
\newcommand{\mat}[1]{\mathbf{#1}}
\newenvironment{myenum}%
{\begin{enumerate}\setlength{\itemsep}{10pt}\setlength{\parskip}{5pt}}%
{\end{enumerate}}
\newcommand{\textred}[1]{\textcolor{red}{#1}}
\newcommand{\textblue}[1]{\textcolor{blue}{#1}}

\begin{document}

\begin{center}
\huge{Response to Reviewers}
\end{center}

We thank the reviewers for their thorough inspection of, and insightful comments on the manuscript. By incorporating their comments, recommendations, and corrections we believe the manuscript is stronger than before. Changes specific to reviewer 1 are marked with text color red, while remarks specific to reviewer 2 are marked with blue. 

\section*{\textbf{Reviewer 1:}}

\textit{The paper is well written, clear, and original. It is suitable for publication in JCP.}

\textred{Thank you for these comments.}

\begin{itemize}

\item \textit{I would say ``... compressible Euler equations."}

\textred{Done. Also we altered the title with the recommended clarification.}

\item \textit{Definition (1.4) refer to equation (1.3). However, (1.3) is for the 1D case but in (1.3) v, w and other contributions which appear in 3D are present. I think that part needs some rewording and changes.}

\textred{Somewhat counterintuitively the one dimensional version of the ideal MHD equations still contains contributions from the tangential directions, such as $v$ and $w$.}

\item \textit{After definitions (1.4): I would say explicitly what divergence-free condition Godunov meant.}

\textred{We have expanded the discussion of the divergence-free condition.}

\item \textit{This sentence does not sound well: ``In order for the numerical solver to remain applicable to flows that may develop discontinuities the entropy conservative flux acts as a baseline." It is not very clear.}

\textred{That is true, the original wording was cumbersome. We have changed this sentence to be clearer.}

\item \textit{``... a naive design of the dissipation term...". Does this mean that the approaches used in [5] and [23] are naive? I am asking this because the sentence beginning with ``The averaging procedure ..." is connected to the next one through ``However, ..."}

\textred{True, the word naive in this instance can carry a negative connotation. We have removed the work naive and instead state that the dissipation term needs to carefully constructed for high Mach number flows.}

\item \textit{After equation (2.2): missing ``where" before $R$}

\textred{Fixed.}

\item \textit{After equation (2.5): I would say in words that $h$ is the enthalpy}

\textred{Done.}

\item \textit{``Because entropy conservative schemes produce high-frequency oscillations near shocks (see e.g. [23]), additional dissipation is added to the baseline EC flux to ensure that the entropy inequality (2.6) is satisfied discretely." Adding dissipation does not mean that the oscillations disappear. Furthermore, a numerical solution can be oscillatory but still satisfy the entropy inequality. Thus, oscillations do not have to always be associated to the non satisfaction of the entropy inequality. I believe that the above sentence has to be modified.}

\textred{True, the original wording was misleading. We have modified this sentence to ensure we don't make claims that are untrue.}

\item\textit{Furthermore, I believe that the previous works of Carpenter and collaborators (SIAM 2014 and JCP 2015) for the Euler and Navier-Stokes equations has to be referenced after the above sentence. In fact, those papers provided a clear path to the flourishing studies on entropy conservative and entropy stable algorithm for the other hyperbolic system of equations.}

\textred{Indeed so, the recommended references have been added.}

\item\textit{Nothing is said about positivity. The mapping between conservative and entropy variables is valid if and only if positivity of the complete thermodynamic state is guaranteed. This, should be added to the text.}

\textred{That is true, we have added a sentence about the importance of positivity. Although, positivity is implicitly important (even neglecting the entropy analysis) otherwise the ideal MHD equations are no longer hyperbolic. On the discrete level our previous work in ``A Novel High-Order, Entropy Stable, 3D AMR MHD Solver with Guaranteed Positive Pressure" allows for an entropy stable computational framework where the pressure is guaranteed to be positive.}

\item\textit{What do you mean with the italic ``enough" after equation (2.12)? The sign of the jump of the entropy is known but not the strength.}

\textred{That is a good point. We do not know, on the continuous level, the exact amount of entropy that should be generated at the shock (regardless of strength). Therefore, it is impossible to discretely mimic this entropy generation. So, the word ``enough" is ambiguous. What we meant was that we want to ensure the entropy is dissipated near shocks (as required by physics) but in rarefractions or contact discontinuities we want less dissipation. We have reworded this sentence to reflect this goal where we use scalar dissipation near shocks and matrix dissipation near contacts in the hybrid solver.}

\item\textit{In this section an example of ``bad" averaging technique is shown. However, I have two questions: 1) Is this averaging used in practical situations? If the answer is positive, I recommend to give at least two references. 2) The dissipation should be added to make the scheme robust and at the same time respect the physics of the problem (well, we actually do not know the exact increment of the entropy across a shock). Obviously the flux (3.1) does not respect that because of the equality (2.10) is not satisfied. Can you point this out?}

\textred{These are both good questions. For question one we have added two references where the simple arithmetic mean is used. We have explicitly added the suggested answer for question two in the text.}

\item\textit{You address my question above at the beginning of this section. However, I believe that the most natural place to add the first few sentence of this section is at the end of section 3. It is there where I would expect to see an explanation. I hope you agree.}

\textred{True, that is a better place for such a discussion. We rearranged the sentences appropriately.}

\item\textit{Can you still achieve entropy stability (i.e., equation (2.25)) and still satisfy relation (4.2) for all i if you do not make the operator symmetric?}

\textred{No, we need the operator to be symmetric (and positive definite) such that when we contract into entropy space the dissipation term will be a quadratic form scaled by a negative sign (thus guaranteeing that entropy decreases). If the operator were not symmetric we could no longer guarantee the sign of the term in (2.25) and, therefore, cannot guarantee entropy stability.}

\item\textit{If the magnetic field is zero one gets the compressible Euler equations. In such a case, has the dissipation term in (4.16) a specific form? It looks like it converges to the Chandrashekar dissipation, which as far as I remember is more dissipative than the ES scheme. Am I correct?}

\textred{Yes, if the magnetic field is zero then we return to the kinetic energy preserving and entropy conserving baseline numerical flux of Chandrashekar. The dissipation term in our scheme is very similar to the dissipation term of Chandrashekar (given in the appendix of ``Entropy stable schemes on two-dimensional unstructured grids'' by D. Ray and P. Chandrashekar and U. Fjordholm and S. Mishra.) The only differences are the averaged states used for the discrete enthalpy ($h$) and the discrete sound speed ($a$). We outline the (much simpler) compressible Euler dissipation term in the appendix of this work.}

\item\textit{Can you at least mention if the algorithm in FLASH is a FV, FD, etc. scheme and which is its formal order? I know these information but a reader not familiar to it might not know these details. Can you also mention the time integration scheme used in your simulations?}

\textred{That is a good point. We add specific details of the time integration and FV nature of FLASH at the beginning of Sec. 5.}

\item\textit{How did you choose the CFL?}

\textred{We have added details just before Sec. 5.1 the details of how the time step/CFL coefficient is selected.}

\item\textit{The caption in table 1 is not centered. It does not look good.}

\textred{All captions for figures and tables are now centered.}

\item\textit{The acronym IR is never defined.}

\textred{We now ensure that IR is defined before it is used in the discussion.}

\item\textit{Before present table 2 can you define all the acronyms that appear there (EOC, EC, KEPEC, ES, KEPES)? It would be nice to refresh their meaning.}

\textred{We now define the meaning of EOC in the text where Table 2 is referenced. Additionally, the meaning of each of the acronyms has been appended to the bottom of Table 2 for clarity.}

\item\textit{I would center the caption of Tables 3.}

\textred{Done. All the captions for figures and tables are now centered.}

\item\textit{What does the difference shown in Fig 2 tell us? Which scheme is better? One is ES and the other KEPES. However, the ES might be more accurate.}

\textred{The motivation of this comparison of the two entropy stable schemes was to show that the KEPES scheme produces numerical results indistinguishable (at least in the eyeball norm) from the scheme we previously implemented into FLASH. Essentially we wanted to ensure we answered the question that may arise: ``You switched the baseline EC flux and the dissipation term, are the results affected in any significant way?" That is the purpose of Fig. 2(b) to show that two schemes don't produce identical flows (as one would expect) but the two flows are very similar.}

\item\textit{Robustness, last word: I think it should be test case (singular)}

\textred{We consider the explosion test in $1D$ and $3D$ as well as our variant on the wind tunnel with a step. So there are three test cases.}

\item\textit{Only in the ``Explosion in a moving ambient medium" case the acronym IR is defined. You should move it up. Thanks.}

\textred{That is a good point. We now define the acronym IR earlier in Sec. 5.1.1.}

\item\textit{``The widely used numerical scheme of Ismail and Roe (IR) can break down for certain flow configurations." Could you please explain why you write ``widely used"? Which researchers use the pure IR scheme which is entropy conservative? What if you use IR with your dissipation? I think this part has to be clarified. It is not clear which is the IR schemes and who uses it.}

\textred{We have added references to help clarify what is meant by ``widely used''. We simply meant that many research groups are using the baseline EC flux of IR as well as some dissipation term to guarantee entropy stability. We don't know of any researchers that use the pure IR scheme simply because for the Euler equations one is interested in shocks. Therefore, the pure entropy conservative scheme is not physically relevant.}

\textred{If we use IR with the most robust dissipation terms developed in this paper we still encounter robustness issues (as we demonstrated in our recent paper ``A novel averaging technique for discrete entropy stable dissipation operators for ideal MHD''). This is due to the particular parametrization vector $\vec{z}$ used in the IR flux. The parametrization cause the mass flux component to depend on pressure, which causes issues for the explosion in a moving ambient medium we consider here. So for these initial conditions one typically finds in astrophysics the IR scheme with the parametrization is not well suited (unless you are willing to shrink the CFL number considerably).}

\item\textit{Fig. 3: the color bars are difficult to interpret. Could you please explain them?}

\textred{The FLASH framework has full AMR capabilities in $1D$, $2D$, or $3D$. The color bar represents the refinement level in the $1D$ mesh for the blast wave test case. It mainly serves to note that the AMR refinement is able to track the strong shock with a moving background medium.}

\item\textit{Wind tunnel with a step: say that the inflow is supersonic (of course) so that a non expert understand that everything has to be imposed at the left boundary. Do you agree?}

\textred{We have clarified the discussion of the initialization.}

\item\textit{Fig. 5: say in words in the text that is shows the pressure contour.}

\textred{Fixed.}

\item\textit{Through the numerical test section the IR scheme is used for comparison. I believe you should say something about it in the conclusion.}

\textred{Done. We have added a brief discussion in the conclusions that the new entropy stable scheme is directly compared to our old entropy stable implementation which used the IR type flux.}

\item\textit{Please, clarify which is the IR scheme. Somehow the IR scheme is trashed and this needs a fair and good explanation. Thus, clarity is essential.}

\textred{We've clarified when the IR scheme used is the one we outlined in detail in our previous FLASH paper. That way the error analysis is directly comparable. We also clarify that there is not a robustness issue for flows that don't have fast moving explosions.}

\item\textit{Be consistent in the references. In some references the names are spelled, in some other not.}

\textred{Fixed, now all references use abbreviated names.}

\end{itemize}

\section*{\textbf{Reviewer 2:}}

\textit{The paper deals with entropy consistent numerical flux functions for MHD. The presented flux belongs to a class which is based on an entropy conservative central flux term to which a viscosity type term is added. The presented numerical results indicate that the new numerical flux function is of high quality. I especially appreciate that the CFL-number has not to be reduced in contrary to many other solvers of this type.}

\textblue{We thank the reviewer for these nice comments.}

\begin{itemize}

\item\textit{The authors should point out more clearly what the differences are to their previous work. It is difficult for the reader to find out about that without having read the cited papers. Especially in Section 2.1 it is nearly impossible to find out which of the material is cited from the previous papers and what is new.}

\textblue{We have added additional discussion at the beginning of Sec. 2.1 to put the focus of the current work into context with previous work.}

\item\textit{At some places, the authors cite a paper by Tadmor [19]. According to that paper most commonly used Riemann solvers (Godunov, Osher, Roe with entropy fix,. . . ) are entropy consistent. The above mentioned class of solvers is introduced as an additional class of entropy consistent solvers. Thus, the authors should spend at least one paragraph in order to explain how the new solver fits into the framework of [19] and why they chose that special class of solvers.}

\textblue{That is true, the current work is related to the general entropy stability framework introduced by Tadmor. However, Tadmor's framework is built with dissipation terms that are defined by path integrals in phase space. This is convenient for the mathematical theory but in practice it is numerically expensive (particularly for high-order computations). Therefore, the current work seeks a computationally affordable alternative and defines the particular mean states to be used when buidling matrix-type dissipation terms. We have altered the discussion in the introduction to make this motivation and connection to Tadmor's work clearer.}

\item\textit{The paragraph before Section 2.1 ends with ``. . . additional dissipation is added to the baseline EC flux to ensure that the entropy inequality (2.6) is satisfied discretely." Since the baseline flux is already entropy conserving, this is no meaningful statement. I would suggest to drop everything after ``EC flux".}

\textblue{We have edited the statement as suggested.}

\item\textit{On page 2, middle line, it should be ``It has been shown . . . that it is possible. . . ".}

\textblue{Fixed.}

\item\textit{On page 3, the explanation of the logarithmic mean is a bit short. Although the reader would guess to know what is meant, it would be more clear if the denominator of the mean was explained more clearly.}

\textblue{We have added a sentence explaining that the form of the logarithmic mean arises from an application of the mean value theorem.}

\item\textit{Page 4, second line after formula (2.): Since the baseline flux already satisfies the entropy condition, it should be ``. . . numerical flux still fulfills the entropy inequality. . . ".}

\textblue{Fixed.}

\item\textit{After equation (2.12), the authors should state for which matrix the eigenvectors are computed.}

\textblue{That is a good suggestion to avoid confusion for the reader. We have moved the discussion of the matrix eigendecomposition to be right after (2.12).}

\item\textit{It is difficult to see what happened to the jump indicator in $\Xi$ after equation (2.15).}

\textblue{We have renamed the hybrid solver variable, $\Xi$, to simply be a pressure indicator to avoid confusion with the use of the previous jump operator notation.}

\item\textit{At the end of page 15 the abbreviation IR is used before it is introduced.}

\textblue{We've ensured that the abbreviation is introduced earlier to avoid confusion.}

\item\textit{In the second paragraph on page 11, the authors give the CFL-number they used in their tests. It would be nice to find some information on how this compares to other entropy consistent schemes of the same class.}

\textred{I DON'T KNOW WHAT TO ADD IN THE MAIN TEXT FOR THIS. MABE CITE WAAGAN AS WELL AS US?}

\textblue{From our previous investigations in ``A Novel High-Order, Entropy Stable, 3D AMR MHD Solver with Guaranteed Positive Pressure" we found that the other entropy stable schemes available in FLASH were able to run with similar CFL numbers as the new implementation.}

\end{itemize}

\end{document}
